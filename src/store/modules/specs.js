import { reqspecscount, reqspecslist } from "../../request/api"
const state = {
    // 1.初始值
    list: [],
    // 总数
    total: 0,
    //一页的个数
    size: 2,
    //页码
    page: 1
}
const getters = {
    // 2.导出数据
    list(state) {
        return state.list
    },
    total(state) {
        return state.total
    },
    size(state) {
        return state.size
    },
}
const mutations = {
    // 3.修改list
    changeList(state, list) {
        state.list = list;
    },
    //修改总数
    changeTotal(state, total) {
        state.total = total
    },
    //修改页码
    changePage(state, page) {
        state.page = page
    },

}
const actions = {
    // 4.请list
    reqList({ commit, dispatch, state: { page, size } }) {

        reqspecslist({ page, size }).then(res => {
            if (res.data.code == 200) {
                // 取回来的数据如果是空数组，并且不是第一页，那么需要page-1 ，再请列表
                if (res.data.list.length == 0 && page !== 1) {
                    commit("changePage", page - 1)
                    dispatch("reqList")
                    return;
                }
                //修改
                commit("changeList", res.data.list)
            }
        })
    },
    //请总数
    reqTotal({ commit }) {
        reqspecscount().then(res => {
            if (res.data.code == 200) {
                //修改总数
                commit("changeTotal", res.data.list[0].total)
            }
        })
    },
    //修改页码
    changePage({ commit, dispatch }, page) {
        //修改页码
        commit("changePage", page);
        //再请列表
        dispatch("reqList")
    }
}
export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}

