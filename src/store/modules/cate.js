import {reqcatelist} from "../../request/api"
const state = {
    // 1.初始值
    list:[]
}
const getters = {
    // 2.导出数据
    list(state){
        return state.list
    }
}
const mutations = {
    //3.修改了数据
    changeList(stata,list){
        state.list=list
    }
}
const actions = {
    //4.请数据
    reqList(context){
        reqcatelist({istree:true}).then(res=>{
            if(res.data.code==200){
                //修改
                context.commit("changeList",res.data.list)
            }
        })
    }
    
}
export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}