export const state={
    //用户信息
    info:sessionStorage.getItem("info")?JSON.parse(sessionStorage.getItem("info")):{}
}
export const getters={
    info(state){
        return state.info
    }
}
export const mutations={
    changeInfo(state,info){
        state.info=info;
    }
}