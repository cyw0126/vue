export default {
    //修改info的逻辑 info={token:""}  info={}
    changeInfo(context,info){
        //修改info
        context.commit("changeInfo",info)

        // 同步到本地存储
        if(info.token){
            sessionStorage.setItem("info",JSON.stringify(info))
        }else{
            sessionStorage.removeItem("info")
        }
        
    }
}