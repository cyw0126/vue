export const toggle= {
    data() {
        return {
            // 1.初始化isshow 控制弹框的出现和消失
            info: {
                isshow: false,
                isAdd: true, //true-添加 false-编辑
            },

        };
    },
    methods: {
        //2.点击了添加按钮
        willAdd() {
            this.info.isshow = true;
            this.info.isAdd = true;
        },
        // 22.将要编辑
        willEdit(id) {
            //弹框的状态
            this.info = {
                isshow: true,
                isAdd: false,
            };
            //通知form该请求1条数据
            this.$refs.form.getOne(id);
        },
    }
}