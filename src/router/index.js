import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "../store"
Vue.use(VueRouter)

// 7.路由独享守卫
function checkedRoute(url, next) {
  if (store.getters.info.menus_url.includes(url)) {
    next()
  } else {
    next("/")
  }
}

export const IndexRoutes = [
  {
    path: "menu",
    component: () => import("../pages/menu/menu.vue"),
    meta: {
      title: "菜单管理"
    },
    beforeEnter(to, from, next) {
      checkedRoute('/menu', next)
    }
  },
  {
    path: "role",
    component: () => import("../pages/role/role.vue"),
    meta: {
      title: "角色管理"
    },
    beforeEnter(to, from, next) {
      checkedRoute('/role', next)
    }
  },

  {
    path: "manage",
    component: () => import("../pages/manage/manage.vue"),
    meta: {
      title: "管理员管理"
    }, beforeEnter(to, from, next) {
      checkedRoute('/manage', next)
    }
  },
  {
    path: "cate",
    component: () => import("../pages/cate/cate.vue"),
    meta: {
      title: "分类管理"
    },
    beforeEnter(to, from, next) {
      checkedRoute('/cate', next)
    }
  },
  {
    path: "specs",
    component: () => import("../pages/specs/specs.vue"),
    meta: {
      title: "商品规格"
    },
    beforeEnter(to, from, next) {
      checkedRoute('/specs', next)
    }
  },
  {
    path: "goods",
    component: () => import("../pages/goods/goods.vue"),
    meta: {
      title: "商品管理"
    }, beforeEnter(to, from, next) {
      checkedRoute('/goods', next)
    }
  },
  {
    path: "vip",
    component: () => import("../pages/vip/vip.vue"),
    meta: {
      title: "会员管理"
    },
    beforeEnter(to, from, next) {
      checkedRoute('/vip', next)
    }
  },
  {
    path: "banner",
    component: () => import("../pages/banner/banner.vue"),
    meta: {
      title: "轮播图管理"
    }, beforeEnter(to, from, next) {
      checkedRoute('/banner', next)
    }
  },
  {
    path: "seckill",
    component: () => import("../pages/seckill/seckill.vue"),
    meta: {
      title: "秒杀活动"
    },
    beforeEnter(to, from, next) {
      checkedRoute('/seckill', next)
    }
  },
]

const routes = [
  {
    path: "/",
    component: () => import("../pages/index/index.vue"),
    children: [
      {
        path: "",
        component: () => import("../pages/home/home.vue")
      },
      ...IndexRoutes
    ]
  },
  {
    path: "/login",
    component: () => import("../pages/login/login.vue")
  },

]

const router = new VueRouter({
  routes
})

router.beforeEach((to,from,next)=>{
  if(to.path=="/login"){
    next();return;
  }
  if(sessionStorage.getItem("info")){
    next();return;
  }
  next("/login")
})
export default router
