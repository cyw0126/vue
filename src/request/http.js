import axios from "axios";
import Vue from "vue";
import qs from "qs";
import {errorAlert} from "../utils/alert"
import store from "../store";

if(process.env.NODE_ENV === "development"){
    Vue.prototype.$pre = "http://localhost:3000"
}else{
    Vue.prototype.$pre=""
}

//请求拦截
axios.interceptors.request.use(config=>{
    if(config.url!=="/api/login"){
        config.headers.authorization=store.getters.info.token;
    }
    return config;
})

//响应拦截
axios.interceptors.response.use(res=>{
    console.group("本次请求地址：" + res.config.url);
    console.log(res);
    console.groupEnd();

    if(res.data.code!==200){
        errorAlert(res.data.msg)
    }

    //res
    res.data.list=res.data.list?res.data.list:[];
    
    //掉线
    if(res.data.msg=="登录已过期或访问权限受限"){
        store.dispatch("changeInfo",{});
        router.replace("/login");
    }
    return res;
})

export function get(url,params={}){
    return axios({
        url,
        params
    })
}

export function post(url, params = {}, isFile = false) {
    let d = null
    if (isFile) {
        d = new FormData();
        for (let key in params) {
            d.append(key, params[key])
        }
    } else {
        d = qs.stringify(params)
    }
    return axios({
        url,
        method: "post",
        data: d
    })
}