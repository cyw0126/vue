import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//css
import "./assets/css/reset.css";
// 2.公共组件
import "./components"
// 3.过滤器
import "./filters"
// element-ui 
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
